*************************
Конфигурирование сервисов
*************************

.. contents:: Содержание

При установке системы через ``amanitabar install`` в рабочее окружение копируется
файл ``config.defaul.yml`` из папки с исходным кодом проекта (см. приложение)

Для изменения конфигурационных настроек нужно использовать утилиту amanitabar:

.. code:: sh

  root@test % amanitabar config --help
  Usage: amanitabar config [OPTIONS] COMMAND [ARGS]...

    Configuration management.

    Options:
      --help  Show this message and exit.

    Commands:
      del   Delete a configuration option.
      set   Set a configuration option.
      show  Show configuration.

Примеры
-------
.. code:: bash

    amanitabar config set ODOO_HOST my.odoo.com
    amanitabar config set PINS_PORT /dev/ttyUSB0

Вывод парамтеров конфигурации
-----------------------------
Команда ``config show`` выводит все параметры, а ``config show PARAM`` только параметры,
в которых есть ``PARAM``. Пример:

.. code:: bash

    root@test tmp % amanitabar config show ODOO_HOST
    ODOO_HOST: 127.0.0.1


Начальная конфигурация
----------------------
Файл конфигурации, клонируемый при создании рабочего окружения:

.. literalinclude:: _static/config.default.yml
   :language: yaml

Настройки сервисов
------------------

Odoo
^^^^
Дополнительные настройки Odoo, не включенный в файл по-умолчанию:

* **ODOO_BUS_ENABLED** - использовать ``/longpolling/poll`` (по-умолчанию = 1). 
  Иногда необходимо отключить поллинг, для этого надо установить это значение в 0.
* **ODOO_SINGLE_DB** - так как  ``/longpolling/poll`` не позволяет выбрать базу, то 
  сервис перед началом поллинга вызывает ``/web/session/authenticate`` для выбора базы.
  В случае, если Odoo работает с одной базой, или используется опция ``db_filter``,
  можно в целях оптимизации включить режим одной базы ``ODOO_SINGLE_DB=1``.