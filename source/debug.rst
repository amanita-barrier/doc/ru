***********************************
Отладка и устранение неисправностей
***********************************

RabbitMQ сервер
###############
Если в логе сервиса следующая ошибка:

.. code:: 

    nameko.messaging:402 - WARNING - Error connecting to broker at amqp://guest:********@localhost:5672/ ([Errno 111] ECONNREFUSED).

то не запущен брокер.

Проверяем статус:

.. code:: 

    systemctl rabbitmq-server status

