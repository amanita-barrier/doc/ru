****
ЧаВо
****

Как отключить все сервисы?
##########################
Сервисы запускаются службой supervisor. 

.. code:: bash

    systemctl stop supervisor
    systemctl disable supervisor

Вторая команда отключает автозагрузку сервисов.
