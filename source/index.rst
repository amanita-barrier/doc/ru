Доброе пожаловать в документацию по Amanita Barriers!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :titlesonly:

   cli
   config
   update
   api
   db
   faq
   debug


Установка
=========
Установка зависимостей
----------------------

.. code:: bash

  apt install python3-pip python3-eventlet python3-wheel
  apt install python3-setproctitle python3-psutil
  apt install ansible

Установка Amanita Barrier
-------------------------

.. code:: bash

  pip3 install amanita-barrier


Инициализация рабочего окружения 
--------------------------------
Рабочее окружение - это директория, в которой хранятся:

* Конфигурационный файл ``config.yml``
* База данных приложения ``app.db``
* Лог работы сервисов ``amanita_barrier.log``.

Директория рабочего окружения задается либо флагом ``--prefix``, либо установкой
переменной окружения ``AMANITA_BARRIER_INSTALL_PREFIX``:

.. code:: bash

  amanitabar install --prefix=/srv/barrier

Далее надо инициализировать базу данных:

.. code:: bash

  cd /srv/barrier
  amanitabar db init

**Примечание**: при вызове любой команды утилиты ``amanitabar`` необходимо либо
каждый раз указывать путь к конфигурационному файлу при помощи флага 
``--config=/amanitabar/install/prefix/config.yml`` или уже находиться в рабочей
директори, и последнем случае флаг ``--prefix`` можно не указывать, так как по-умолчанию поиск 
осуществляется в текущей директории.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
