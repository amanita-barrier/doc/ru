Описание команд утилиты управления amanitabar
=============================================
Справочная информация встроена в утилиту amanitabar и доступна по ключу ``--help``:

.. code:: bash

    root@test tmp % amanitabar --help
    Usage: amanitabar [OPTIONS] COMMAND [ARGS]...

      Amanita Barrier management utility.

    Options:
      --config TEXT  Absolute path to configuration file config.yml, if not
                     specified current directory is searched.
      --version      Show version and exit.
      --help         Show this message and exit.

    Commands:
      config   Configuration management.
      db       Database management.
      install  Install required dependencies and prepare working environment.
      log      Show service logs.
      restart  Restart a service.
      run      Manually run a service.
      start    Starts a service if not started.
      status   Show services status.
      stop     Stops a running service.

Пример вызова справочной информации по разделу ``db``:

.. code:: bash

    root@test tmp % amanitabar db --help
    Usage: amanitabar db [OPTIONS] COMMAND [ARGS]...

      Database management.

    Options:
      --help  Show this message and exit.

    Commands:
      init     Initialize database in current working environment.
      upgrade  Run database migration procedure.

